# NFC voting setup written in C

## Dependencies

### NFC

All information at http://libnfc.googlecode.com

### LPD8806 Led Drivers

More info at https://github.com/fishkingsin/elinux-lpd8806

### libCurl

Information at http://stackoverflow.com/questions/11471690/curl-h-no-such-file-or-directory

## Compile code

Make sure to have the lpd8806led.o file on the path and libfnc and curl installed

    gcc -o NFCScanner NFCScanner.c -lnfc -lcurl lpd8806led.o -lm

## running

Load spi modules

    sudo modprobe spi_bcm2708

Requires root to run, use bootscript and double check that module for leds is loaded (spi).
Other common issues are a broken URL and missing dependencies or arguments.

Check the available devices with

    lsusb

Example to run from bootscript.sh

    sudo screen -d -m ./NFCScanner acr122_usb:001:007 likes.txt http://server/url/goes/here 8 param=1

# More Info

## prepare sd card

    sudo diskutil unmountDisk /dev/disk1
    sudo dd bs=1m of=/dev/rdisk1 if=pi-image.img

## update date

Shouldn't be needed anymore. Just need to * 1000

    apt-get install ntpdate

## find ip address

Use the below

    brew install nmap
    ./nmap X.Y.Z.0-255 -p22 --open


## register the bootscript

Instructions on getting the bootscript installed on the rpi.

	sudo cp bootscript.sh /etc/init.d/bootscript
	sudo chmod 755 /etc/init.d/bootscript
	sudo update-rc.d bootscript defaults


# config overview

Moved to another file since this has to remain private.


